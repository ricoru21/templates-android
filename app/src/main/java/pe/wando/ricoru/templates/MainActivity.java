package pe.wando.ricoru.templates;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btn_login_1 = (Button) findViewById(R.id.btn_login_1);
        btn_login_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, Login1Activity.class);
                startActivity(i);
            }
        });
        Button btn_login_2 = (Button) findViewById(R.id.btn_login_2);
        btn_login_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, Login2Activity.class);
                startActivity(i);
            }
        });
        Button btn_login_3 = (Button) findViewById(R.id.btn_login_3);
        btn_login_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, Login3Activity.class);
                startActivity(i);
            }
        });
        Button btn_login_4 = (Button) findViewById(R.id.btn_login_4);
        btn_login_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, Login4Activity.class);
                startActivity(i);
            }
        });

    }
}
