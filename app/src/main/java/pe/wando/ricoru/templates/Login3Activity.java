package pe.wando.ricoru.templates;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

public class Login3Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login3);

        final LinearLayout layout_login_form = (LinearLayout) findViewById(R.id.layout_login_form);
        Glide.with(Login3Activity.this)
                .load(R.drawable.fondo_4)
                .asBitmap()
                .fitCenter()
                .into(new SimpleTarget<Bitmap>(400, 400) {
                    @Override
                    public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                        Drawable fondo = new BitmapDrawable(getResources(), bitmap);
                        layout_login_form.setBackground(fondo);
                        layout_login_form.setAlpha(0.85f);
                    }
                });

    }
}
