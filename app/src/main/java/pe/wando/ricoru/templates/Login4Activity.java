package pe.wando.ricoru.templates;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

public class Login4Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login4);

        final LinearLayout layout_login_form = (LinearLayout) findViewById(R.id.layout_login_form);
        final AppCompatButton sign_up_button = (AppCompatButton) findViewById(R.id.sign_up_button);
        //como el bacground es una imagen gran y pesada para este caso si es recomendable utilizar glide
        Glide.with(Login4Activity.this)
                .load(R.drawable.fondo_1)
                .asBitmap()
                .into(new SimpleTarget<Bitmap>(300, 300) {
            @Override
            public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                Drawable fondo = new BitmapDrawable(getResources(), bitmap);
                layout_login_form.setBackground(fondo);
            }
        });

    }
}
